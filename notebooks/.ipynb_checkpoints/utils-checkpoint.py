'''

utils.py

@author: amanmajid

'''

#---
# import modules
import sys
sys.path.insert(0,'..')

import os
import pandas as pd
import numpy as np
# import config


#---
# util functions

def sum_world(df):
    idx = df.index.intersection(df.index)
    
    incomplete_idx = df.isna().sum(axis=0) > 2
    df.loc['World',:] = df.loc[idx,:].sum(axis=0)
    df.loc['World',:] = df.loc[idx,:].sum(axis=0)
    df.loc['World', incomplete_idx] = np.nan
    
    return df

def check_directory(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

def TidyTable(DataTable):
    '''Convert DataTable into tidy format
    '''
    metadata = DataTable.meta
    TidyTable = DataTable.reset_index().melt(id_vars=['region'],var_name='year')
    TidyTable.meta.update(metadata)
    TidyTable.index.name = None
    return TidyTable