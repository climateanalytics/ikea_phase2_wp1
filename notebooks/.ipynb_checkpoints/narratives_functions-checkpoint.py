

import numpy as np  # Used for creating rating / indicator functions
import pyam  # Useful for storing climate / energy scenario data
import pandas as pd

import pandas_indexing
from pandas_indexing import isin, ismatch, assignlevel

import data_shepherd as ds
from data_shepherd.utils import convert_unit

import pycountry

import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import matplotlib.patches as mpatches
import seaborn as sns
sns.set_palette("colorblind")


#%% GHG Emissions Budget 

def isExtrapolation(
    carbonData,
    pathway, 
    var, 
    startYear, 
    endYear
):
    """
    Checks if the given range is an extrapolation of carbonData - returns True if it is
    """
    years = carbonData.filter(pathway=pathway, variable=var).year
    
    if startYear < years[0]:
        print(f"ERROR: startYear can be no earlier than {years[0]}")
        return True
    elif endYear > years[-1]:
        print(f"ERROR: endYear can be no later than {years[-1]}")
        return True
    else:
        return False

def makeFriendlyToInterpolate(carbonData, pathway, var, axis=0, startYear=2020, endYear=2100):
    """
    Takes a descripton of the pathway and emission variable of interest and prepares a dataframe in a form amenable for the pandas.DataFrame.interpolate function. 
    Returns a new dataframe with NaNs emission values for each missing year.

    Parameters
    ----------
    pathway : the name of the pathway whose carbon budget you are interested in, as a string.  Currently only 5 pathways are supported: modify the pathways variable definition and rerun notebook to change.
    var : the gas variable to be interpolated. as a string
    axis : optional, 0 makes the returned df vertical, and 1 makes it horizontal
    startYear : year to start counting emissions budget from
    endYear: date emissions budget should last until
    """
    if isExtrapolation(carbonData, pathway, var, startYear, endYear):
        return pd.DataFrame()
    
    # adding to a dictionary is more efficient than modifying the size of a dataframe
    emissionsDict = carbonData.filter(pathway=pathway, variable=var).timeseries().to_dict(orient='list') # drop index information
    readyDict = {}
    
    # assemble dictionary with blank spaces for interpolated data
    for year in range(startYear, endYear+1):
        if year in emissionsDict.keys():
            readyDict[year] = emissionsDict[year][0]
        else: # no data is available for this year
            readyDict[year] = np.nan
    
    # convert to dataframe
    if axis == 0: # vertical
        readyDict = {"Year" : list(readyDict.keys()), var : list(readyDict.values())}
        emissionsData = pd.DataFrame(readyDict)
    else:   # horizontal
        emissionsData = pd.DataFrame([readyDict])
        
    return emissionsData


def calculateCarbonBudget(carbonData, pathway, gasType, estimationMethod, startYear=2020, endYear=2100):
    """
    Integrate Mt GHG emissions per year to estimate a given pathway's net carbon budget in Gt GHG.
    Sequestered emissions are subtracted from total budget.

    Parameters
    ----------
    pathway : A pathway contained in the carbonData set, as a string
    gasType : An emissions rate variable in the carbonData set, as a string
    estimationMethod : a string "left", "right", or other indicating whether to integrate using the lefthand, righthand, or average y-value for box heights
    startYear : The year to start calculating emissions budget from, no earlier than 2005
    endYear : default=2100, the year the carbon budget should last until, no later than 2100

    Notes:
    Does NOT support LowEnergyDemand pathway
    """
    # confirm estimation method is a valid option
    if estimationMethod.lower()[:4] not in ['righ', 'left', 'trap', 'inte']:
        print(f'{estimationMethod} is NOT a supported estimation method.  Select \'left\', \'right\', \'trapezoid\', or \'interpolate\'')
    estimationMethod = estimationMethod.lower()[:4]

    # check that the timeframe for the carbon budget is not an extrapolation of available data
    if isExtrapolation(carbonData, pathway, gasType, startYear, endYear):
        return pd.DataFrame()
        
    # Bring in the pathway data
    years = carbonData.filter(pathway=pathway, variable=gasType).year
    if estimationMethod == 'inte': # interpolate carbon budget
        # interpolate data
        intermedEmissions_df = makeFriendlyToInterpolate(carbonData,pathway, gasType, axis=1, startYear=years[0], endYear=years[-1])
        intermedEmissions_df.interpolate(method='linear', limit_direction='both', axis=1, inplace=True)
        # calculate carbon budget
        iStart = startYear-years[0]
        iEnd = endYear-years[0]+1
        carbonBudget = (intermedEmissions_df.iloc[[0], iStart:iEnd].sum(axis=1)/1000)[0]
        return carbonBudget
    else: # make a rougher estimate
        intermedEmissions_df = carbonData.filter(pathway=pathway, variable=gasType).timeseries()
        
    emissions = intermedEmissions_df.to_numpy()[0]   
    #years = list(intermedEmissions_df.to_dict().keys())
    carbonBudget = 0

    # Limit # Integrate emissions over the years
    s = years.index(startYear)
    e = years.index(endYear)
    emissions = emissions[s:e+1]
    years = years[s:e+1]

    # Select lefthand, rightand, or trapizoid integration
    for i in range(len(emissions)-1):
        yearDiff = years[i+1] - years[i]
        if estimationMethod == 'left':
            GHGpyr = emissions[i] # LEFT
        elif estimationMethod == 'righ':
            GHGpyr = emissions[i+1] # RIGHT
        elif estimationMethod == 'trap':
            GHGpyr = (emissions[i] + emissions[i+1])/2 # TRAPEZOID  
        else:
            GHGpyr= np.nan
            print("Error: this block should be unreachable")
        carbonBudget += yearDiff*GHGpyr
    return carbonBudget/1000


def summarizeCarbonBudget(carbonData, gas='Emissions|Kyoto Gases', startYear=2020, endYear=2100, estMethods = ['t', 'l', 'r', 'i']):
    """ 
    Print summary of estimated carbon budgets given a greenhouse gas from startYear to endYear

    Parameters
    ----------
    gas : emissions variable indicating which gas you want an estimated budget for
    startYear : when does the budget start? (default is 2020)
    endYear : how long should this budget last until? (default is 2100)
    estMethods : list of which estimation methods to print results for
    """
    for path in list(carbonData.filter(variable=gas).meta.pathway):
        summaryString = f'{path} has a {startYear}-{endYear} {gas} budget of:'
        if 'i' in estMethods:
            interp = calculateCarbonBudget(carbonData, path, gas, 'interpolate', startYear=startYear).round(3)
            summaryString += f'\n \t* {interp} Gt GHG (linearly interpolated)'
        if 'l' in estMethods:
            left = calculateCarbonBudget(carbonData, path, gas, 'left', startYear=startYear).round(3)
            summaryString += f'\n \t* {left} Gt GHG (lefthand)'
        if 't' in estMethods:
            trap = calculateCarbonBudget(carbonData, path, gas, 'trap', startYear=startYear).round(3)
            summaryString += f'\n \t* {trap} Gt GHG (trapezoid)'            
        if 'r' in estMethods:
            right = calculateCarbonBudget(carbonData, path, gas, 'right', startYear=startYear).round(3)
            summaryString += f'\n \t* {right} Gt GHG (righthand)'              
        print(summaryString + '\n')


#%% Carbon Sequestration

def plotSequestrationStack(carbonData, rows=2, cols=3):
    """
    Show stacked plot of energy sources used in the given sector for each pathway.  Only supports 4 pathways.

    Parameters
    ----------
    DATA : optional alternative input dataframe, use DATA=regional_data for downscaled graphics
    """
    region='World'
    validPaths = list(carbonData.filter(variable='*Carbon Sequestration|CCS*', level = '2-').meta.pathway)
    
    # prep subplots
    f, axes = plt.subplots(nrows=rows,
                           ncols=cols,
                           figsize=(25,13),
                           sharey=False)
    axr = axes.ravel()

    for count, ax in enumerate(axr[:len(validPaths)]):
        plt.sca(ax)
        pathway = validPaths[count]
    
        data = carbonData.filter(pathway=pathway, 
                              variable=carbonData.filter(variable=f'Carbon Sequestration|*').variable,
                                year = [2020,2030,2040,2050, 2060,2070,2080,2090,2100], 
                                  #region = region
                                    )
        data = data.filter(variable = [
            
             #'Carbon Sequestration|CCS',
             'Carbon Sequestration|CCS|Biomass',
             'Carbon Sequestration|CCS|Fossil',
             'Carbon Sequestration|Direct Air Capture',
             'Carbon Sequestration|CCS|Industrial Processes',
             'Carbon Sequestration|Land Use|Afforestation',
             'Carbon Sequestration|Enhanced Weathering',
             #'Carbon Sequestration|Land Use',
             'Carbon Sequestration|Land Use|Biochar',
             'Carbon Sequestration|Land Use|Soil Carbon Management',
             'Carbon Sequestration|Other',
             'Carbon Sequestration|Feedstocks'
            
                                      ])
        data.convert_unit('Mt CO2/yr', 'Gt CO2/yr', factor=1e-3, inplace=True)

        # plot the data
        data.plot.stack(ax=ax, legend = False)
        
        ax.set_title(f'{carbonData.filter(pathway=pathway).model[0]} \n {carbonData.filter(pathway=pathway).scenario[0]}') 
        ax.set_ylabel("Gt of CO2 / year")

    #legend
        #if pathway == ('MESSAGEix-GLOBIOM|LowEnergyDemand'):
    if len(validPaths) % 3 == 0:
        anchor = [-0.5, -0.2]
    elif len(validPaths) % 3 == 1:
        anchor = [1.5, -0.2]
    else:
        anchor = [0.5, -0.2]
    plt.tight_layout(pad=0.9)
    plt.legend(loc="center",
            bbox_to_anchor=anchor,
            ncol=5)
    
    plt.suptitle(f'Carbon Sequestration', x=0.5, y=1.02, fontsize='xx-large', fontweight='bold')
    plt.show()

#%% Sectoral energy mix

def computeSectorShares(DATA, sector):
    """
    Helper function modifies a given Pyam dataframe by adding share of final energy columns for a given sector.
    Sector can be Industry, Residential and Commercial, Transportation, or Other Sector.
    Also computes aggregate biomass column

    Parameters
    ----------
    DATA : either regional_data or global_data
    sector : one of the following strings ['Industry|', 'Residential and Commercial|', 'Transportation|', 'Other Sector|', ''], where '' indicates all sectors
    """

    if '|' in sector : 
        sector = sector.replace('|', '')
    
    biomass
    if sector != "" : 
        if f'Final Energy|{sector}|Biomass' not in DATA.variable:
            DATA.aggregate(f'Final Energy|{sector}|Biomass', 
                           [f'Final Energy|{sector}|Gases|Biomass', f'Final Energy|{sector}|Liquids|Biomass', f'Final Energy|{sector}|Solids|Biomass'],
                         method='sum',
                         append=True)
            DATA.check_aggregate('Final Energy|{sector}Biomass')
    else : 
        if f'Final Energy|{sector}Biomass' not in DATA.variable:
            DATA.aggregate(f'Final Energy|{sector}Biomass', 
                           [f'Final Energy|{sector}Gases|Biomass', f'Final Energy|{sector}Liquids|Biomass', f'Final Energy|{sector}Solids|Biomass'],
                         method='sum',
                         append=True)
            DATA.check_aggregate('Final Energy|{sector}Biomass')
        
    # all sectors
    sectorVariables = []
    # for sec in sector : 
    if len(sector) > 1:
        if f'Final Energy|{sector}|Biomass' not in DATA.variable:
            DATA.aggregate(f'Final Energy|{sector}|Biomass', 
                           [f'Final Energy|{sector}|Gases|Biomass', f'Final Energy|{sector}|Liquids|Biomass', f'Final Energy|{sector}|Solids|Biomass'],
                         method='sum',
                         append=True)
            DATA.check_aggregate('Final Energy|{sector}|Biomass')

        sectorVariables.extend(DATA.filter(variable=f'Final Energy|{sector}|*').variable)


        # share of variable
        for var in sectorVariables: 
            if var+'|share' not in DATA.variable:
                DATA.divide(
                        var, 
                        '|'.join(var.split('|')[:2]), #Final Energy|{sector}
                        var + '|share', 
                        append = True)
            
    else:
        if f'Final Energy|{sector}Biomass' not in DATA.variable:
            DATA.aggregate(f'Final Energy|{sector}Biomass', 
                           [f'Final Energy|{sector}Gases|Biomass', f'Final Energy|{sector}Liquids|Biomass', f'Final Energy|{sector}Solids|Biomass'],
                         method='sum',
                         append=True)
            DATA.check_aggregate('Final Energy|{sector}Biomass')
            
        sectorVariables = DATA.filter(variable=f'Final Energy|{sector}*', level = 2).variable
        
        # share of variable
        for var in sectorVariables: 
            if var+'|share' not in DATA.variable:
                DATA.divide(
                        var, 
                        'Final Energy',
                        var + '|share', 
                        append = True)
            
        # adjust for use of percentages instead of proportions
    DATA.convert_unit("","%",factor=1e2,inplace=True)  


def computeShares(DATA, total = True):
    """
    Helper function creates percentage variables for all energy sources in each sector of global_data
    Also prepares component variables for an all-sectors end-use energies composition.

    Parameters
    ----------
    DATA: either global_data or regional_data
    """
    sectors = ['Industry', 'Residential and Commercial', 'Transportation', 'Other Sector']
    
    # create overall final energy heat variable
    if 'Final Energy|Heat' not in DATA.variable:
        DATA.aggregate('Final Energy|Heat', 
                       ['Final Energy|Industry|Heat', 'Final Energy|Residential and Commercial|Heat', 'Final Energy|Transportation|Heat', 'Final Energy|Other Sector|Heat'],
                       method='sum',
                       append=True)
        DATA.check_aggregate('Final Energy|Heat')

    # create overall other final energy variable
    if 'Final Energy|Other' not in DATA.variable:
        DATA.aggregate('Final Energy|Other', 
                       ['Final Energy|Industry|Other', 'Final Energy|Residential and Commercial|Other', 'Final Energy|Transportation|Other'],
                       method='sum',
                       append=True)
        DATA.check_aggregate('Final Energy|Other')
         

    # compute shares for each sector
    for sector in sectors:
        # if f'Final Energy|{sector}|Electricity|share' not in DATA.variable:
        computeSectorShares(DATA, sector)

    if total:
        #compute share of total end-use sectors
        sectorVariables = [
         'Final Energy|Biomass',
         'Final Energy|Electricity',
         'Final Energy|Gases|Natural gas',
         'Final Energy|Heat',
         'Final Energy|Hydrogen',
         'Final Energy|Liquids|Oil and Other',
         #'Final Energy|Other',
         'Final Energy|Solids|Coal'
        ]
        for var in sectorVariables: 
            if var + '|share' not in DATA.variable:
                DATA.divide(
                        var, 
                        'Final Energy',
                        var + '|share', 
                        append = True) 
        DATA.convert_unit("","%",factor=1e2,inplace=True)  
    
    # return DATA


def plotSectorEnergySources(sector, DATA, pathways, region='World', rows=2, cols=3):
    """
    Show stacked plot of energy sources used in the given sector for each pathway.

    Parameters
    ----------
    sector : One of the following strings: 'Transportation', "Industry', 'Residential and Commercial' (still working out issues for '')
    DATA : optional alternative input dataframe, use DATA=regional_data for downscaled graphics
    region : a region (string) as specified in the REMIND model, defaults to 'World'
    rows: how many rows of subplots should be displayed (int)
    cols : how many columns of subplots should be displayed (int)
    """

    if len(sector) > 0:
        sector = sector+'|'
    else : 
        sector = ""
    
    if f'Final Energy|{sector}Biomass|share' not in DATA.variable:
        computeSectorShares(DATA, sector)
    
    # handle number of graphs available for given region
    goodPaths = pathways
    if region == 'World':
        goodPaths = pathways
          
    elif region in DATA.filter(model='MESSAGEix-GLOBIOM').region:
        goodPaths = [x for x in pathways if 'MESSAGEix-GLOBIOM' in x]
        row=1
        
    elif region in DATA.filter(model='REMIND-MAgPIE 2.1-4.2').region:
        goodPaths = [x for x in pathways if not 'MESSAGEix-GLOBIOM' in x]

    else: 
        print(f'{region} is not an accepted region name')
        goodPaths = pathways
        region = 'World'

    # scale = 1 # scaling factor for the plot
    # subplot_abs_width = 2*scale # Both the width and height of each subplot
    # subplot_abs_spacing_width = 0.2*scale # The width of the spacing between subplots
    # subplot_abs_excess_width = 0.3*scale # The width of the excess space on the left and right of the subplots
    # subplot_abs_excess_height = 0.3*scale # The height of the excess space on the top and bottom of the subplots

    # fig_width = (cols * subplot_abs_width) + ((cols-1) * subplot_abs_spacing_width) + subplot_abs_excess_width # 25
    # fig_height = subplot_abs_width+subplot_abs_excess_height #13

    # prep subplots
    f, axes = plt.subplots(nrows=rows,
                           ncols=cols,
                           figsize=(25,13),
                           sharey=False)
    axr = axes.ravel()

    for count, ax in enumerate(axr[:len(goodPaths)]):
        plt.sca(ax)
        pathway = goodPaths[count]
    
        data = DATA.filter(pathway=pathway, 
                              variable=DATA.filter(variable=f'Final Energy|{sector}*|share').variable,
                                   year = [2020,2030,2040,2050, 2060,2070,2080,2090,2100], 
                                  region = region
                                 ) #.timeseries().reset_index() #timeseries changes dataframe to from pyam to pandas
        data = data.filter(variable = [  #.set_index('variable').reindex([
     
              
        f'Final Energy|{sector}Other|share', #violet
        f'Final Energy|{sector}Solids|Coal|share', #slategray
        f'Final Energy|{sector}Liquids|Oil and Other|share', #gold
        f'Final Energy|{sector}Gases|Natural gas|share', #coral
        #f'Final Energy|{sector}|Solids|share',
        f'Final Energy|{sector}Hydrogen|share', #mediumorchid
        f'Final Energy|{sector}Heat|share', #purple
        #f'Final Energy|{sector}|Liquids|share',
        f'Final Energy|{sector}Biomass|share', #lightskyblue
        #f'Final Energy|{sector}|Liquids|Biomass|share', #skyblue
        #f'Final Energy|{sector}|Gases|Biomass|share', #deepskyblue
        #f'Final Energy|{sector}|Solids|Biomass|share', #lightskyblue
        f'Final Energy|{sector}Electricity|share', #mediumseagreen
        #f'Final Energy|{sector}|Gases|share'
        ])
        
        pyam.run_control().update( # set colors
            {'color': {'variable' : {f'Final Energy|{sector}Other|share' : 'violet',
                                     f'Final Energy|{sector}Solids|Coal|share' : 'slategray',
                                     f'Final Energy|{sector}Liquids|Oil and Other|share' : 'gold',
                                     f'Final Energy|{sector}Gases|Natural gas|share' : 'coral',
                                     f'Final Energy|{sector}Hydrogen|share' : 'mediumorchid',
                                     f'Final Energy|{sector}Heat|share' : 'purple',
                                     f'Final Energy|{sector}Biomass|share' : 'lightskyblue',
                                     f'Final Energy|{sector}Electricity|share' : 'mediumseagreen'
                                     }}})
        
        data.plot.stack(ax=ax, legend = False)
        
        ax.set_title(f'{data.filter(pathway=pathway).model[0]} \n {data.filter(pathway=pathway).scenario[0]}') 
        ax.set_ylabel("% of total final energy")
        ax.set_ylim(0,100)

    #legend
        #if pathway == ('MESSAGEix-GLOBIOM|LowEnergyDemand'):
    if len(goodPaths) % 3 == 0:
        anchor = [-0.5, -0.2]
    elif len(goodPaths) % 3 == 1:
        anchor = [1.5, -0.2]
    else:
        anchor = [0.5, -0.2]
    plt.tight_layout(pad=0.9)
    plt.legend(loc="center",
            bbox_to_anchor=anchor,
            ncol=5)

    if len(sector) > 1:
        title = f'{region}: {sector[:-1]}'
    else:
        title = region
    plt.suptitle(title, x=0.5, y=1.02, fontsize='xx-large', fontweight='bold')
    plt.show()



#%% Phase-out dates

def computeTotalFF_bySector(Data, sector):
    """
    Helper function creates a new variable representing the percentage of total final energy demand composed of fossil fuels for the given sector.

    Parameters
    ---------
    Data : either global_data or regional_data
    sector: one of the following strings: ['Industry', 'Residential and Commercial', 'Transportation', 'Other Sector']
    
    Notes:
    Aggregate final energy fossil fuel phase-out is not supported.  To find information on primary energy fossil fuel reliance by sector, look to the datatoolbox.
    """
    Data.aggregate(f'Final Energy|{sector}|Total Fossil|share', 
                   [f'Final Energy|{sector}|Gases|Natural gas|share', f'Final Energy|{sector}|Liquids|Oil and Other|share', f'Final Energy|{sector}|Solids|Coal|share'],
                 method='sum',
                 append=True)


def computeTotalFF(Data):
    """
    Helper function creates a new variable representing the percentage of total final energy demand composed of fossil fuels for each sector.

    Parameters:
    Data : either global_data or regional_data
    """
    sectors = ['Industry', 'Residential and Commercial', 'Transportation', 'Other Sector']
    for sector in sectors:
        if f'Final Energy|{sector}|Total Fossil|share' not in Data.variable:
            computeTotalFF_bySector(Data, sector)

def findFirstYearBelowThreshold(info, scenario, var, threshold): # info should be how ?? 
    """
    Find the year when the fossil fuel reliance of a sector in the given pathway is below the given threshold for the first time.
    Returns estimated year of phase-out as an integer, or -1 if the variable of interest is not phased-out within the dataset, called info.

    Parameters:
    -----------
    info : filtered fossil fuel data from global_data or regional_data
    scenario : pathway scenario, one of the following: 
    ['LowEnergyDemand', 'CEMICS_SSP1-1p5C-minCDR', 'NGFS2_Net-Zero 2050 - IPD-95th', 'SusDev_SSP1-PkBudg900', 'DeepElec_SSP2_ HighRE_Budg900']
    var : variable whose phase-out date you are interested in
    threshold : value used to define phase-out, as a percentage
    """
    #print(scenario, sector, threshold)
    LED_trans = info.filter(scenario = scenario, variable=var).timeseries().to_numpy()
    belowOnePercent = LED_trans < threshold
    filter = np.where(belowOnePercent)
    year = [2020,2030,2040,2050, 2060,2070,2080,2090,2100] #2000, 2005, 2010, 
    if len(filter[0]) > 0:
        x = year[filter[1][0]]
    else:
        #print(f'No-phase out for {scenario, sector}')
        x = -1
    return x


def plotPathwayFossilUsage(INPUTdata, pathways, region = 'World', rows=2, cols=3, threshold=2.5):
    """
    Show line plot of fossil fuel usage (coal, oil, and gas) by sector.
    
    Parameters:
    -----------
    INPUTdata : either global_data or regional_data (use regional_data to look at end-use fossil fuel reliance in downscaled regions)
    region : a region as listed in the regional_data set.  Both REMIND and MESSAGE regions are supported.
    threshold : percentage fossil fuel usage should drop below before dashed red phase-out line can be drawn
    """

    # compute Total Fossil variables if needed
    if 'Final Energy|Industry|Total Fossil|share' not in INPUTdata.variable:
        computeTotalFF(INPUTdata)

    # handle number of graphs available for given region
    if region == 'World':
        goodPaths = pathways
          
    elif region in INPUTdata.filter(model='MESSAGEix-GLOBIOM').region:
        goodPaths = [x for x in pathways if 'MESSAGEix-GLOBIOM' in x]
        row=1
        
    elif region in INPUTdata.filter(model='REMIND-MAgPIE 2.1-4.2').region:
        goodPaths = [x for x in pathways if not 'MESSAGEix-GLOBIOM' in x]

    else: 
        print(f'{region} is not an accepted region name')
        goodPaths = pathways
        region = 'World'

    
    # prep subplots
    f, axes = plt.subplots(nrows=rows,
                           ncols=cols,
                           figsize=(25,13),
                           sharey=False)
    axr = axes.ravel()
    
    for count, ax in enumerate(axr[:len(goodPaths)]):
        plt.sca(ax)
        pathway = goodPaths[count]
        scenario = pathway.split("|")[1]
    
        data = INPUTdata.filter(
            pathway=pathway, 
            variable=INPUTdata.filter(variable=f'Final Energy|*|Total Fossil|share').variable,
            year=list(range(2020,2110,10)), 
            region=region)
        
        data = data.filter(variable = [    
              
         'Final Energy|Industry|Total Fossil|share',
         'Final Energy|Residential and Commercial|Total Fossil|share',
         'Final Energy|Transportation|Total Fossil|share',
         #'Final Energy|Other Sector|Total Fossil|share'
            
        ])

        # set colors
        sectorColors = {'Final Energy|Industry|Total Fossil|share' : 'darkred',
                                     'Final Energy|Residential and Commercial|Total Fossil|share' : 'forestgreen',
                                     'Final Energy|Transportation|Total Fossil|share' : 'royalblue',
                                     #'Final Energy|Other Sector|Total Fossil|share' : "aqua"
                                     }
        pyam.run_control().update( 
            {'color': {'variable' : sectorColors }}
        )

        # plot the data
        data.plot.line(ax=ax, linewidth=3, legend = False)

        # make vertical phase-out bars
        sectors = {'Industry' : 'darkred', 'Residential and Commercial' : 'forestgreen', 'Transportation' : 'royalblue'}
        for sector in sectors.keys():
            phaseOutYear = findFirstYearBelowThreshold(data, scenario, f'Final Energy|{sector}|Total Fossil|share', threshold)
            if phaseOutYear > 0:
                ax.axvline(x=phaseOutYear, color='r', linewidth = 1, linestyle='dashed') #sectors[sector]

        # axis labels
        ax.set_title(f'{INPUTdata.filter(pathway=pathway).model[0]} \n {INPUTdata.filter(pathway=pathway).scenario[0]}') 
        ax.set_ylabel("% of sector final energy coming from fossil fuels")

    #legend
        if pathway == ('REMIND-MAgPIE 2.1-4.2|SusDev_SSP1-PkBudg900'):
            plt.tight_layout(pad=0.9)
            plt.legend(loc="center",
                    bbox_to_anchor=[1.5,-0.2],
                    ncol=5) 
    plt.suptitle(f"Fossil Fuel Reliance by Sector: {region}", x=0.5, y=1.02, fontsize='xx-large', fontweight='bold')
    plt.show()


#%% Power energy mix - FF relience

def computeSumofSubs(Data, SumName, SubVariables):
    """
    General helper function creates a new variable representing the sum of SubVariables.
    Modifies the given DataFrame to include the aggregated variable.

    Parameters:
    Data : global_data or regional_data dataframe
    SumName : name of variable to be created
    SubVariables : list of component variables, as strings
    """
    # Sum of sub-variables
    Data.aggregate(SumName, SubVariables,
                 method='sum',
                 append=True)


def computeShareofSum(Data, SumName, SubVariables, DenominatorVariable, sumNotPresent=True):
    """
    Create a new variable representing the percentage of DenominatorVariable made up by each subVariable and the sum of SubVariables.

    Parameters:
    -----------
    Data : global_data or regional_data dataframe
    SumName : variable naming the aggregation of subVariables
    SubVariables : a list of variables that make up a subset of DenominatorVariable
    DenominatorVariable : basis for percentage share
    sumNotPresent : boolean indicating whether to create the SumName variable
    """
    # Sum of sub-variables
    if sumNotPresent:
        computeSumofSubs(Data, SumName, SubVariables)
        
    # Share of main variable
    SubVariables.append(SumName)
    for var in SubVariables: 
        Data.divide(
                    var, 
                    DenominatorVariable, 
                    var+'|share', 
                    append = True)
    Data.convert_unit("","%",factor=1e2,inplace=True)  


def computeElecFossilShares(Data, ccs=False):
    """
    Find the share fossil fuel-based secondary electricity makes up of all secondary elecricity.  Graphs reliance on coal, oil, and gas for generating electricity.

    Parameters
    ----------
    Data : global_data or regional_data dataframe
    ccs : boolean value: if True, treats fossil fuels with CCS as distinct from fossil fuels without CCS.  If False, does not consider the use of CCS.
    """
    if ccs:
        subVariables = [
             'Secondary Energy|Electricity|Coal|w/ CCS',
             'Secondary Energy|Electricity|Coal|w/o CCS',
             'Secondary Energy|Electricity|Gas|w/ CCS',
             'Secondary Energy|Electricity|Gas|w/o CCS',
             'Secondary Energy|Electricity|Oil|w/ CCS',
             'Secondary Energy|Electricity|Oil|w/o CCS',
        ]
    else:
        subVariables = [
            'Secondary Energy|Electricity|Coal',
            'Secondary Energy|Electricity|Gas',
            'Secondary Energy|Electricity|Oil'
        ]
    
    ## Find shares
    if f'{subVariables[0]}|share' not in Data.variable:
        denominatorVariable = 'Secondary Energy|Electricity'
        sumName = 'Secondary Energy|Electricity|Fossil'
        sumPresent = 'Secondary Energy|Electricity|Fossil|share' in Data.variable #if False, will find sum
        computeShareofSum(Data, sumName, subVariables, denominatorVariable, sumNotPresent = not sumPresent)
    else:
        print("Shares of fossil fuel in electricity are already computed")

def plotElectricityFossilUsage(INPUTdata, pathways, region='World', rows=2, cols=3, threshold=2.5, ccs=False):
    """
    Show line plot of fossil fuel usage (coal, oil, and gas) in generating electricity.

    Parameters
    ----------
    INPUTdata : global_data or regional_data DataFrame
    region : region abbreviation as listed in REMIND or MESSAGE model
    threshold : percentage below which sector-spefific fossil fuel usgae should drop below before it can be said to have been "phased-out"
    ccs : bool value indicating whether to consider fossil fuels with CCS as seprate from those without it
    """
    # handle number of graphs available for given region
    if region == 'World':
        goodPaths = pathways
          
    elif region in INPUTdata.filter(model='MESSAGEix-GLOBIOM').region:
        goodPaths = [x for x in pathways if 'MESSAGEix-GLOBIOM' in x]
        row=1
        
    elif region in INPUTdata.filter(model='REMIND-MAgPIE 2.1-4.2').region:
        goodPaths = [x for x in pathways if not 'MESSAGEix-GLOBIOM' in x]

    else: 
        print(f'{region} is not an accepted region name')
        goodPaths = pathways
        region = 'World'
    
    f, axes = plt.subplots(nrows=rows,
                           ncols=cols,
                           figsize=(25,13),
                           sharey=False)
    axr = axes.ravel()

    if ccs:
        computeElecFossilShares(INPUTdata, ccs=True)
    else:
        computeElecFossilShares(INPUTdata)
    
    for count, ax in enumerate(axr[:len(goodPaths)]):
        plt.sca(ax)
        pathway = goodPaths[count]
        scenario = pathway.split("|")[1]
    
        data = INPUTdata.filter(
            pathway=pathway, 
            variable=INPUTdata.filter(variable='Secondary Energy|Electricity*share').variable,
            year = [2020,2030,2040,2050, 2060,2070,2080,2090,2100], 
            region=region)
        # prep colors
        fuels = {}
        fuelColors = {
             'Secondary Energy|Electricity|Coal|share' : 'slategrey',
             'Secondary Energy|Electricity|Coal|w/ CCS|share' : 'lightgrey',
             'Secondary Energy|Electricity|Coal|w/o CCS|share' : 'darkgrey',
             #'Secondary Energy|Electricity|Fossil|share' : '',
             'Secondary Energy|Electricity|Gas|share' : 'coral',
             'Secondary Energy|Electricity|Gas|w/ CCS|share' : 'peru',
             'Secondary Energy|Electricity|Gas|w/o CCS|share' : 'coral', 
             'Secondary Energy|Electricity|Oil|share' : "gold",
             'Secondary Energy|Electricity|Oil|w/ CCS|share' : 'lemonchiffon',
             'Secondary Energy|Electricity|Oil|w/o CCS|share' : 'gold'            
            }
        
        # select variables
        if ccs:
            data = data.filter(variable = [
                 #'Secondary Energy|Electricity|Coal|share',
                 'Secondary Energy|Electricity|Coal|w/ CCS|share',
                 'Secondary Energy|Electricity|Coal|w/o CCS|share',
                 #'Secondary Energy|Electricity|Fossil|share',
                 #'Secondary Energy|Electricity|Gas|share',
                 'Secondary Energy|Electricity|Gas|w/ CCS|share',
                 'Secondary Energy|Electricity|Gas|w/o CCS|share',
                 #'Secondary Energy|Electricity|Oil|share',
                 'Secondary Energy|Electricity|Oil|w/ CCS|share',
                 'Secondary Energy|Electricity|Oil|w/o CCS|share'
            ])            
            for var in fuelColors.keys():
                if "CCS" in var:
                    fuels[var] = fuelColors[var]
        else:
            data = data.filter(variable = [
                 #'Secondary Energy|Electricity|Fossil|share',
                 'Secondary Energy|Electricity|Oil|share',
                 'Secondary Energy|Electricity|Gas|share',
                 'Secondary Energy|Electricity|Coal|share'
                ])
            for var in fuelColors.keys():
                if ("CCS" not in var) and ("Fossil" not in var):
                    fuels[var] = fuelColors[var]

        # set colors
        pyam.run_control().update( 
            {'color': {'variable' : fuelColors}}
        )

        # plot the data
        data.plot.stack(ax=ax, legend = False)

        # make vertical phase-out bars
        #fuels = {'Coal' : 'slategrey', 'Gas' : 'coral', 'Oil' : 'gold'}
        for fuel in fuels.keys():
            phaseOutYear = findFirstYearBelowThreshold(data, scenario, fuel, threshold)
            if phaseOutYear > 0:
                ax.axvline(x=phaseOutYear, color=fuels[fuel], linewidth = 1, linestyle='dashed')

        # axis labels
        ax.set_title(f'{INPUTdata.filter(pathway=pathway).model[0]} \n {INPUTdata.filter(pathway=pathway).scenario[0]}') 
        ax.set_ylabel("% of sector secondary electricity coming from fossil fuels")

    #legend
        if pathway == ('REMIND-MAgPIE 2.1-4.2|SusDev_SSP1-PkBudg900'):
            plt.tight_layout(pad=0.9)
            plt.legend(loc="center",
                    bbox_to_anchor=[1.5,-0.2],
                    ncol=5) 
    plt.suptitle(f"Fossil Fuel Reliance in Electricity Generation: {region}", x=0.5, y=1.02, fontsize='xx-large', fontweight='bold')
    plt.show()


def computeFossilShareofFinalElec(Data, computeComplement=False):
    """ 
    Compute the percentage reliance on coal, gas, and oil in generating electrcity.  This is a form of indirect fossil fuel usage that should be considered when calculating total fossil fuel reliance in the power sector.
    Assumes Final Energy|Electricity|Fossil = Final Energy|Electricity * (Secondary Energy|Electricity|Fossil / Secondary Energy|Electricity)"

    Parameters
    ----------
    Data : DataFrame to modify, either global_data or regional_data
    computeComplement : if true, will compute share of secondary electrcity NOT generated by fossil fuels
    """
    # Find share of electricity in total final energy
    if 'Final Energy|Electricity|share' not in Data.variable:
        Data.divide(    'Final Energy|Electricity', 
                        'Final Energy', 
                        'Final Energy|Electricity|share', 
                        append = True)
        Data.convert_unit("","%",factor=1e2,inplace=True) 

    # Find fossil-makeup of sector final energy
    sectors = ['Industry|', 'Residential and Commercial|', 'Transportation|', 'Other Sector|', '']
    # variables which will be used
    shareFossilInElec = 'Secondary Energy|Electricity|Fossil|share' # percentage of electricity coming from fossil fuels
    shareNotFFInElec = 'Secondary Energy|Electricity|NotFF|share' # percentage of electricity NOT coming from fossil fuels
    if shareFossilInElec not in Data.variable:
        computeElecFossilShares(Data, ccs=True)
    if computeComplement and shareNotFFInElec not in Data.variable:
        Data.subtract(1, shareFossilInElec, shareNotFFInElec, append=True)
    
    for sector in sectors: 
        
        # variables which will be used
        sectorTotalElec = f'Final Energy|{sector}Electricity' # total amount of electricity used in final sector
        sectorShareElec = f'Final Energy|{sector}Electricity|share' # share of final energy that is electricity in given sector
        
        # variables to be created
        finalElecFossil = f'Final Energy|{sector}Electricity|Fossil' # amount of electricity coming from fossil fuel in given sector
        finalElecFossilShare = f'Final Energy|{sector}Electricity|Fossil|share' # share of sector energy which is fossil-fuel-based electricity
        finalElecNotFF = f'Final Energy|{sector}Electricity|NotFF' # amount of electricity NOT coming from fossil fuels in given sector
        finalElecNotFFShare = f'Final Energy|{sector}Electricity|NotFF|share' # share of sector energy which is NOT fossil-fuel-based electricity
        
        # create product variables
        if finalElecFossil not in Data.variable:
            print(f"(amount of electricity used in {sector}) * (percentage makeup of fossil fuels in electricity) = (amount of fossil-based electricity used in {sector})")
            Data.multiply(sectorTotalElec, shareFossilInElec, finalElecFossil, append=True)
        if finalElecFossilShare not in Data.variable:
            print(
                f"(percentage utlization of electricity in {sector}) * (percentage makeup of fossil fuels in electricity) = (percentage of fossil-based electricity used in {sector})")
            Data.multiply(sectorShareElec, shareFossilInElec, finalElecFossilShare, append=True)
        if computeComplement and finalElecNotFF not in Data.variable:
            print(
                f"(amount of electricity used in {sector}) * (percentage makeup of non-fossil fuels in electricity) = (amount of non-fossil-based electricity used in {sector})")
            Data.multiply(sectorTotalElec, shareNotFFInElec, finalElecNotFF, append=True)
        if computeComplement and finalElecNotFFShare not in Data.variable:
            print(
                f"(percentage utlization of electricity in {sector}) * (percentage makeup of non-fossil fuels in electricity) = (percentage of non-fossil-based electricity used in {sector})")
            Data.multiply(sectorShareElec, shareNotFFInElec, finalElecNotFFShare, append=True)
    
    Data.convert_unit('% ** 2', '%', factor=1e-2, inplace=True)
    Data.convert_unit('% * EJ / a', 'EJ / a', factor=1e-2, inplace=True)

#%% AFOLU

def giveAFOLU_CO2e(global_data):
    """
    Convert methane and nitrous oxide to CO2 equivalent.
    Does NOT modify Data.  Returns dataset of AFOLU CO2e emissions

    Parameters:
    GHG_df : DataFrame containing information on AFOLU GHG emissions, such as global_data
    """
    # Convert GHGs to CO2e
    CH4_df = global_data.filter(variable=['Emissions|CH4|Agriculture','Emissions|CH4|LULUCF']).convert_unit('Mt CH4/yr', to='Mt CO2e/yr', context='AR5GWP100')
    N2O_df = global_data.filter(variable=['Emissions|N2O|Agriculture','Emissions|N2O|LULUCF']).convert_unit('kt N2O/yr', to='Mt CO2e/yr', context='AR5GWP100')
    CO2_df = global_data.filter(variable=['Emissions|CO2|Agriculture','Emissions|CO2|LULUCF']).convert_unit('Mt CO2/yr', to='Mt CO2e/yr', context='AR5GWP100')
    pGHG_df = pyam.concat([CH4_df, N2O_df])
    GHG_df = pyam.concat([pGHG_df, CO2_df])

    # Find sum of CO2 equivalent produced in each sector
    if 'Emissions|CO2e|LULUCF' not in GHG_df.variable:
        computeSumofSubs(GHG_df, 
                        'Emissions|CO2e|LULUCF',
                        ['Emissions|CH4|LULUCF', 'Emissions|N2O|LULUCF', 'Emissions|CO2|LULUCF'])
        computeSumofSubs(GHG_df, 
                        'Emissions|CO2e|Agriculture',
                        ['Emissions|CH4|Agriculture', 'Emissions|N2O|Agriculture', 'Emissions|CO2|Agriculture'])
    ghg_info = GHG_df.filter(variable = ['Emissions|CO2e|LULUCF', 'Emissions|CO2e|Agriculture'])
    
    ghg_info.convert_unit('Mt CO2e/yr',"Gt CO2e/yr",factor=1e-3,inplace=True)  
    return ghg_info


def plotAFOLU_CO2e(global_data, pathways, rows=2, cols=3):
    """
    Show stacked plot of LULUCF and Agriculture GHG emissions for each pathway
    """
    ghg_info= giveAFOLU_CO2e(global_data)
    
    f, axes = plt.subplots(nrows=rows,
                           ncols=cols,
                           figsize=(25,13),
                           sharey=False)
    axr = axes.ravel()

    n = len(pathways)
    for count, ax in enumerate(axr[:n]):
        plt.sca(ax)
        pathway = pathways[count]
    
        data = ghg_info.filter(pathway=pathway, 
                                  variable=['Emissions|CO2e|LULUCF', 'Emissions|CO2e|Agriculture'],
                                  #year = [2020,2030,2040,2050, 2060,2070,2080,2090,2100], 
                                  region='World'
                                 )
        data.plot.stack(ax=ax, legend = False)
        
        ax.set_title(f'{ghg_info.filter(pathway=pathway).model[0]} \n {ghg_info.filter(pathway=pathway).scenario[0]}') 
        ax.set_ylabel("Gt of CO2 equivalent / year")

        #legend
        if pathway == ('MESSAGEix-GLOBIOM|LowEnergyDemand'):
            plt.tight_layout(pad=0.9)
            plt.legend(loc="center",
                    bbox_to_anchor=[0.5,-0.2],
                    ncol=5)
    
    plt.suptitle("CO2e Emissions from AFOLU", x=0.5, y=1.02, fontsize='xx-large', fontweight='bold')
    plt.show()


def prepareAFOLUPieData(global_data, pathway, yr):
    """
    Helper function uses global_data to prepare AFOLU emissions information for AFOLUPieChart() function.
    Returns numpy array of emissions and the filtered DataFrame from global_data

    Parameters
    ----------
    pathway : net zero pathway of interest as a string
    yr : date to display emissions for
    """    
    data = global_data.filter(pathway=pathway, 
         variable=[
         #'Emissions (excl. LULUCF)',
         #'Emissions CO2 (excl. AFOLU)',
         # 'Emissions|CH4|AFOLU',
         'Emissions|CH4|Agriculture',
         'Emissions|CH4|LULUCF',
         # 'Emissions|CO2|AFOLU',
         'Emissions|CO2|Agriculture',
         'Emissions|CO2|LULUCF',
         # 'Emissions|N2O|AFOLU',
         'Emissions|N2O|Agriculture',
         'Emissions|N2O|LULUCF'
        ], 
        year = yr
        ).timeseries()
    
    emissions = []
    for i in range(0, len(data), 2):
        emissions.append(([data.iloc[i,0], data.iloc[i+1,0]]))
    emissions = np.array(emissions)
    return emissions, data


def AFOLUPieChart(global_data, pathway, year):
    """
    Generate a pie chart indicating the emissions from a given pathway for a given year.
    Not recommended.

    Parameters
    ----------
    pathway : net zero pathway of interest as a string
    yr : date to display emissions for
    """
    emissions, data = prepareAFOLUPieData(global_data, pathway, year)
    ## make the plot
    fig, ax = plt.subplots()

    size = 0.3
    vals = emissions
    
    cmap = plt.colormaps["tab20c"]
    outer_colors = cmap(np.arange(3)*4)
    inner_colors = cmap([1, 2, 5, 6, 9, 10])
    
    # labels
    outer_labels = [data.index[i*2][4] for i in range(len(data)//2)]
    inner_labels = [data.index[i][3].split('|')[2] for i in range(len(data))]
    
    # outer ring
    ax.pie(vals.sum(axis=1), radius=1, colors=outer_colors, labels=outer_labels,
       wedgeprops=dict(width=size, edgecolor='w'))
    
    # inner ring
    ax.pie(vals.flatten(), 
       radius=1-size, 
       colors=inner_colors, 
       #labels=inner_labels, 
       autopct='%1.1f%%',
       wedgeprops=dict(width=size, edgecolor='w')
      )
    
    ax.set(aspect="equal", title='AFOLU Gas Emission Sources`')
    plt.legend(outer_labels+inner_labels, loc='center right', bbox_to_anchor=(-0.1, 1.),
           fontsize=8)
    plt.tight_layout()
    plt.show()