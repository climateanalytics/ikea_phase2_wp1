#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# %%
"""
Created on Mon Mar 21 09:12:19 2022

@author: ageiges
"""

import datatoolbox as dt
import numpy as np
import utils


#%% Config
release_year = '2022'

energy_source = 'IEA_WEB_DETAILED_2021'
co2_emission_source = 'IEA_GHG_FUEL_DETAILED_2021'


#%% Computing functions

def compute_historic_reneable_share(energy_source):

    inp = dict()
    inp['re']   = dt.getTable(f'Secondary Energy|Electricity|Renewables__Historic__{energy_source}')
    inp['total'] = dt.getTable(f'Secondary Energy|Electricity__Historic__{energy_source}')
    
    for key in inp.keys():
        inp[key] =  utils.sum_world(inp[key])
    
    # renOutput = 
    # totalOutput = dt.getTable(f'Secondary Energy|Electricity__Historic__{energy_source}')
    
    RE_share_historic = inp['re']  / inp['total']   
    RE_share_historic = RE_share_historic.convert('%')
    
    RE_share_historic.meta.update({'entity' : 'Share»|Renewables',
                                     'category' : 'Secondary Energy|Electricity',
                                     'pathway' : 'Historic',
                              'source' : 'IEA|CAT_computed'})
    
    return RE_share_historic



def compute_coal_share(energy_source):

    inp = dict()
    inp['coal']   = dt.getTable(f'Secondary Energy|Electricity|Coal, peat and oil shale__Historic__{energy_source}')
    inp['total'] = dt.getTable(f'Secondary Energy|Electricity__Historic__{energy_source}')
    
    for key in inp.keys():
        inp[key] =  utils.sum_world(inp[key])
    coal_share_historic = inp['coal'] / inp['total']   
    coal_share_historic = coal_share_historic.convert('%')
    
    coal_share_historic.meta.update({'entity' : 'Share|Coal',
                                     'category' : 'Secondary Energy|Electricity',
                                     'pathway' : 'Historic',
                              'source' : 'IEA|CAT_computed'})
    
    return coal_share_historic



def compute_historic_energy_intensity(energy_source,
                                      co2_emission_source):
    
    inp = dict()
    inp['emissions']   = dt.getTable(f'Emissions|CO2|combustion|Electricity__Historic__{co2_emission_source}')  
    inp['emissions'].meta['unit'] += '/yr'
    inp['generation'] =dt.getTable(f'Secondary Energy|Electricity__Historic__{energy_source}')
    
    for key in inp.keys():
        inp[key] =  utils.sum_world(inp[key])
        

    
    emission_intensity = inp['emissions'] / inp['generation']   
    emission_intensity = emission_intensity.convert('g CO2 / kWh')
    
    emission_intensity.meta.update({'entity' : 'Emission_intensity',
                                     'category' : 'Secondary Energy|Electricity',
                                     'pathway' : 'Historic',
                                     'source' : 'IEA|CAT_computed'})
    
    return emission_intensity





def compute_historic_gas_share(energy_source):

    inp = dict()
    inp['gas']   = dt.getTable(f'Secondary Energy|Electricity|Natural gas__Historic__{energy_source}')
    inp['total'] = dt.getTable(f'Secondary Energy|Electricity__Historic__{energy_source}')
    
    for key in inp.keys():
        inp[key] =  utils.sum_world(inp[key])
    

    gas_share_historic = inp['gas']  / inp['total']   
    gas_share_historic = gas_share_historic.convert('%')
    
    gas_share_historic.meta.update({'entity' : 'Share|Gas',
                                     'category' : 'Secondary Energy|Electricity',
                                     'pathway' : 'Historic',
                              'source' : 'IEA|CAT_computed'})
    
    return gas_share_historic



#%% Computing results

# results dataset
results = dt.TableSet()


re_share = compute_historic_reneable_share(energy_source)
re_share.generateTableID()
results.add(re_share)

em_int = compute_historic_energy_intensity(energy_source, co2_emission_source)
em_int.generateTableID()
results.add(em_int)

gas_share = compute_historic_gas_share(energy_source)
gas_share.generateTableID()
results.add(gas_share)

coal_share = compute_coal_share(energy_source)
coal_share.generateTableID()
results.add(coal_share)
#%% Save output

# full output 
for key in results.keys():
    # cleaning
    isna_idx = results[key].isnull().all(axis=0)
    cols_to_keep = results[key].columns[~isna_idx]
    results[key] = results[key].loc[:,cols_to_keep]
results.to_compact_excel(f'full_power_sector_historic_{release_year}.xlsx')

#SoCA output  that is only the global  
for key in results.keys():
    # cleaning
    results[key] = results[key].loc[['World'],:]
    isna_idx = results[key].isnull().all(axis=0)
    cols_to_keep = results[key].columns[~isna_idx]
    results[key] = results[key].loc[:,cols_to_keep]
 

results.to_compact_excel(f'SoCa_power_sector_historic_{release_year}.xlsx')
