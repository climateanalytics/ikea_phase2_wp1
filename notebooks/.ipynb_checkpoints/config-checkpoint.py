'''

global_variables.py

@author: amanmajid

'''

#---
# import modules
import sys
sys.path.insert(0,'..')

import matplotlib as mpl

from utils import *

#---
# define global variables

# dir_path
dir_path = {
    'output_directory_data'     : '../outputs/data/',
    'output_directory_figures'  : '../outputs/figures/',
}

# 1.5 degree compatible pathways
compatible_pathways = [
    'IMA15-LiStCh|IMAGE 3.0.1',
    'CD-LINKS_NPi2020_1000|WITCH-GLOBIOM 4.4',
    'CD-LINKS_NPi2020_400|WITCH-GLOBIOM 4.4',
    'ADVANCE_2020_1.5C-2100|MESSAGE-GLOBIOM 1.0',
    'SSP1-19|AIM_CGE 2.0',
    'SSP2-19|AIM_CGE 2.0',
    'TERL_15D_LowCarbonTransportPolicy|AIM_CGE 2.1',
    'TERL_15D_NoTransportPolicy|AIM_CGE 2.1',
]


# generations types to sum for renewables
renewables_generation_types = [
    'Secondary Energy|Electricity|Biomass',
    'Secondary Energy|Electricity|Hydro',
    # 'Secondary Energy|Electricity|Nuclear',
    'Secondary Energy|Electricity|Solar',
    'Secondary Energy|Electricity|Wind'
]


# generations types to sum for UNABATED coal
unabated_coal_generation_types = [
    'Secondary Energy|Electricity|Coal|w_o CCS'
]


# check for directories
for k in dir_path.keys():
    check_directory(dir_path[k])


# plot settings
mpl.rcParams['lines.linewidth']     = 1
mpl.rcParams['lines.linestyle']     = '-'
mpl.rcParams['font.family']         = 'sans-serif'
mpl.rcParams['font.sans-serif']     = 'Arial'
mpl.rcParams['savefig.bbox']        = 'tight'
mpl.rcParams['savefig.dpi']         = 600
